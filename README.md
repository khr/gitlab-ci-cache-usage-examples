# GitLab CI Cache Usage Examples

This repo contains usage examples for the GitLab CI cache and is referenced
in the [Bits and Bytes edition 210 from August 2022](https://docs.mpcdf.mpg.de/bnb/210.html#gitlab-ci-distributed-cache).

Two examples are shown, the second being specific to MPCDF shared runners:

1. Python CI with cached third-party dependency
2. CUDA CI pipeline, building the CUDA code on a CPU runner and using the cache to forward the build to a GPU runner to execute the test there
