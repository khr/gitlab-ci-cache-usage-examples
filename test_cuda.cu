#include <cstdio>

__global__ void cuda_hello(){
    printf("Hello from GPU!\n");
}

int main() {
    cuda_hello<<<1,1>>>();
    cudaDeviceSynchronize();
    return (cudaSuccess != cudaGetLastError());
}
